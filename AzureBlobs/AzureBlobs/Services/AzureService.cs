﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using System.IO;

namespace AzureBlobs.Services
{
    public class AzureService
    {
        public void SaveFileToBlob(string fileName, Stream stream)
        {
            var connectionString = CloudConfigurationManager.GetSetting("StorageConnectionsString");
            var cloudStorageAccount = CloudStorageAccount.Parse(connectionString);

            var cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
            var cloudBlobContainer = cloudBlobClient.GetContainerReference("containerexample");
            cloudBlobContainer.CreateIfNotExists();

            var blob = cloudBlobContainer.GetBlockBlobReference(fileName);
            blob.UploadFromStream(stream);
        }
    }
}